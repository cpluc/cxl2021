#include<iostream>
#include<cstring>
#include<memory>
using namespace std;

/*
*
*	function:	mystring class c++ program demo
*	author:		chen xianlei
*	date:		2021-07-18
*	version:	1.0
*
*/

class mystring
{
public:
	mystring();
	mystring(const char* t_data);
	mystring(const mystring&t_mystring);
	mystring operator=(const mystring&t_mystring);
	~mystring();
	void show()const;
private:
	char* m_data;
};

mystring::mystring()
{
	m_data = new char[1];
	m_data[0] = '\0';
}

mystring::~mystring()
{
	if(m_data != nullptr)
		delete []m_data;
}

mystring::mystring(const char* t_data)
{
	int t_len = strlen(t_data);
	m_data = new char[t_len+1];
	strcpy(m_data, t_data);	
}

mystring::mystring(const mystring&t_mystring)
{
	int t_len = strlen(t_mystring.m_data);
	m_data = new char[t_len+1];
	strcpy(m_data, t_mystring.m_data);
}

mystring mystring::operator=(const mystring&t_mystring)
{
	if(this == &t_mystring)
		return *this;
	delete []m_data;
	int t_len = strlen(t_mystring.m_data);
	m_data = new char[t_len+1];
	strcpy(m_data, t_mystring.m_data);
	return *this;
}

void mystring::show()const
{
	cout<<m_data<<endl;
}

//g++ mystring.cpp -std=c++11
int main()
{
	unique_ptr<mystring>t_mystring1(new mystring("chenxl"));
	t_mystring1->show();

	mystring t_mystring2("chenxl");
	mystring t_mystring3;
	t_mystring3 = t_mystring2;
	t_mystring2.show();
	t_mystring3.show();
	mystring t_mystring4(t_mystring2);
	t_mystring4.show();	
	cout<<"hello mystring..."<<endl;
	return 0;
}
